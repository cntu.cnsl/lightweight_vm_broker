import os
import sys
from loguru import logger as L
from Command import Command
import configparser

CONFIG_PATH=os.path.join("configs", "config")
""" Utility Class."""
class Util():
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read(CONFIG_PATH)

    """ Mount a point in path as private
    point can be: "dev", "sys"
    """
    def mountPrivate(self, path, point):
        cmd = "sudo mount -v --rbind /{} {}/{}".format(point,path,point)
        c = Command(cmd)
        c.run()
        cmd = "sudo mount --make-rprivate {}/{}".format(path, point)
        c = Command(cmd)
        c.run()
        return

    def mountENV(self, path):
        # Mount /dev
        if not os.path.isdir(os.path.join(path, "dev")):
            os.mkdir(os.path.join(path, "dev"))
        self.mountPrivate(path, "dev")

    def unmountENV(self, path):
        if not path:
            return
        if path == "/":
            return
        cmd = "sudo umount -l {}/dev".format(path)
        c = Command(cmd)
        c.run()
        return


    def makedirs(self, path):
        if not os.path.sep in path:
            # There is only one path
            if os.path.isdir(path):
                # Folder exists
                return
            os.makedirs(path)
            return
        # There is more than one folder
        sub_paths = path.split(os.path.sep)
        count = 0
        base_dir = os.getcwd()
        while(count < len(sub_paths)):
            sub_path = sub_paths[:count+1]
            # There is only one path
            if os.path.isdir(sub_path):
                # Folder exists
                continue
            os.mkdir(sub_path)
        return

    def getConfig(self, name):
        return self.config['DEFAULT'][name]

    """ Execute command in alpine """
    def executeInAlpine(self, command):
        fatsystem_path = self.getConfig("fatsystem_path")
        cmd = "sudo chroot {} {}".format(fatsystem_path, command)
        try:
            c = Command(cmd)
            out = c.run()[1].decode('utf-8')
        except:
            L.error("Error executing command: {}".format(cmd))
            return sys.exit(1)
        return out

    """ Find available files in alpine environment. """
    def findInAlpine(self, file, search_type="all"):
        fatsystem_path = self.getConfig("fatsystem_path")
        common_bin_dirs = ["usr/bin", "usr/sbin", "bin", "sbin"]
        common_lib_dirs = ["lib", "usr/lib/"]
        base_dir = os.getcwd()

        # Find libs
        if search_type == "lib" or search_type == "all":
            for _dir in common_lib_dirs:
                check = "{}/{}/{}".format(fatsystem_path, _dir, file)
                if os.path.isfile(check):
                    L.info("File: {} in folder {}".format(file, check))
                    return [check]

        # Find bins
        if search_type == "bin" or search_type == "all":
            for _dir in common_bin_dirs:
                check = "{}/{}/{}".format(fatsystem_path, _dir, file)
                if os.path.isfile(check):
                    L.info("File: {} in folder {}".format(file, check))
                    return [check]

        if search_type == "all":
            # Cannot find file
            L.error("Cannot find file: {}".format(file))
            # Search in folder
            simple_name = file
            if "=" in simple_name:
                simple_name = simple_name.split("=")[0]
            cmd = "find {} -name \"*{}*\"".format(fatsystem_path, simple_name)
            c = Command(cmd)
            candidates = c.run()[1].decode("utf-8")
            L.info("Searching for matches of {}. Candidates: {}".format(simple_name, candidates))
            if search_type == "all":
                # More compatible
                return candidates.strip().split("\n")
            else:
                # More lightweight
                # @Todo: Need more logic on this one, future work
                return []
        return None

    """ Chroot Command for Alpine environment. """
    def chroot_command_alpine(self, command):
        out = None
        fatsystem_path = os.path.join("utils", "alpine", "alpine")
        cmd = "{}/enter-chroot /bin/sh -c \"{}\"".format(fatsystem_path,
                                                         command)
        c = Command(cmd)
        try:
            out = c.run()[1].decode('utf-8')
        except Exception as e:
            L.error("Command Error: {}".format(e))
        return out

