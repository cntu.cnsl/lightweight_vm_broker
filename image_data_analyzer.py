import sys
import os
from loguru import logger as L
import wget
import tarfile
import shutil
from util import Util
from Command import Command

class ImageDataAnalyzer():

    """Initialization. """
    def __init__(self, util, box_type="alpine"):
        self.box_type = box_type
        self.util = util

    """ Extract apps.
    Depending on the type of box (alpine, docker)
    """
    def extractApps(self):
        if self.box_type == "alpine":
            apps = self.extractAlpineApps()
            return apps
        return None

    """ Extract apps in fatsystem.

    Extract apps in the fatsystem environment
    """
    def extractAlpineApps(self):
        app_name = self.util.getConfig('app_name')
        L.info("Extract app {} from fatsystem".format(app_name))
        fatsystem_path = os.path.join("utils", "alpine", "alpine")
        bin_dirs = ["/usr/bin", "/usr/sbin", "/bin", "/sbin"]
        apps = []
        for d in bin_dirs:
            cmd = "{}/enter-chroot /bin/sh -c \"cd {}; apk info -W * | grep '{}-'\"".format(
                    fatsystem_path, d, app_name)
            c = Command(cmd)
            try:
                out = c.run()[1].decode('utf-8').strip()
            except:
                # No information
                continue
            if not out:
                continue
            if "\n" in out:
                # Have more than one candidates
                for item in out.split("\n"):
                    # Extract only the binary locations
                    apps.append(item.split()[0])
                continue
            # Has single
            apps.append(out.split()[0]) # Extract only the binary locations
        return apps

    """ Collect dependencies from a library

    Using ldd
    """
    def collectDepsFromLib(self, fatsystem_path, lib_name):
        # ldd a lib to find the deps
        full_path = self.util.findInAlpine(lib_name, "lib")
        required_libs = []
        if not full_path:
            return None
        for path in full_path:
            cmd = "ldd {}".format(path)
            c = Command(cmd)
            result = c.run()[1].decode("utf-8")
            if ".so" not in result:
                # No library traces in string
                continue
            for line in result.split("\n"):
                if ".so" not in line:
                    # No library traces in line
                    continue
                if "not found" not in line:
                    # The lib has been found already
                    continue
                # else
                lib_name = line.split()[0]
                required_libs.append("so:%s" % lib_name)
        return required_libs

    """ Extract deps in fatsystem.

    Extract apps dependencies in the fatsystem environment

    @input: full path of a binary
    """
    def extractDeps(self, binary):
        L.info("Extract deps from binary: {}".format(binary))
        binary_name = os.path.basename(binary)
        binary_path = os.path.dirname(binary)
        fatsystem_path = os.path.join("utils", "alpine", "alpine")
        cmd = "{}/enter-chroot /bin/sh -c \"cd {}; apk info -R {}\"".format(fatsystem_path,
                binary_path, binary_name)
        c = Command(cmd)
        libs = []
        deps = []
        try:
            out = c.run()[1].decode('utf-8')
            lines = out.split("\n")
            if len(lines) < 2:
                return [],[]
            for index in range(len(lines)):
                if index == 0:
                    continue
                line = lines[index]
                if not line.strip():
                    continue
                if "so:" in line:
                    lib_deps = self.collectDepsFromLib(fatsystem_path, line.split(":")[-1])
                    if lib_deps:
                        libs = libs + lib_deps
                    libs.append(line)
                else:
                    deps.append(line)
        except:
            # No information
            return [],[]
        return libs, deps


    """ Get the correct path of the file """
    def copyData_collectFilePath(self, file, common_dir):
        # Find in common directory
        for d in common_dir:
            full_path = os.path.join(d, file)
            if not os.path.isfile(full_path):
                continue
            return full_path
        # Cannot find in common dir
        # Search
        if self.box_type == "alpine":
            cmd = "find {} -name {}".format("alpine", file)
            c = Command(cmd)
            out = c.run()[1].decode('utf-8').strip()

    """ copyLibs

    Copy libraries to initramfs folder
    """
    def copyBusybox(busybox_compile):
        # Copy the built busybox, later
        busybox_install = os.path.join(busybox_compile, "_install")
        if not os.path.isdir(busybox_install):
            L.error("Busybox has not recompiled yet")
            return None
        # Copy
        cmd = "cp -avR {}/* initramfs/".format(busybox_install)
        c = Command(cmd)
        c.run()

    """ Collect the deps and libs from a binary.

    RECURSIVELY
    """
    def recursiveExtract(self, output, app):
        libs, deps = self.extractDeps(app)
        if not deps:
            # Break of the recursive
            if libs:
                # Copy all dependencies to busybox
                L.info("libs: {}".format(libs))
                for lib in libs:
                    output.append("lib:{}".format(lib))
            return output
        else:
            # Go deeper
            L.info("deps: {}".format(deps))
            # Add lib to output first
            if libs:
                for lib in libs:
                    output.append("lib:{}".format(lib))
            for dep in deps:
                output.append("bin:{}".format(dep))
                output = self.recursiveExtract(output, dep)
        return output

    """ MAIN API """
    def analyze(self):
        apps = self.extractApps()
        app_name = self.util.getConfig('app_name')
        image_apps = {}
        for app in apps:
            # Recursive
            output = self.recursiveExtract([],app)
            fatsystem_path = os.path.join("utils", "alpine", "alpine")
            image_apps["%s%s" %(fatsystem_path, app)] = output
        # Add the package name also (some apps like openssh has different name between binary and package)
        image_apps[app_name] = []
        return image_apps

    """ Dynamic API.

    Execute app to check for "No such file or directory" error
    """
    def analyzeWithExec(self):
        # Mount and unmount
        L.info("Analyzing the execution")
        app_name = self.util.getConfig('app_name')
        executor = self.util.getConfig('execution')
        initramfs_path = self.util.getConfig('initramfs')
        # Execute app with chroot
        cmd = "sudo chroot initramfs {}".format(executor)
        c = Command(cmd)
        # Collect errors
        out = c.run()[2].decode("utf-8")
        for line in out.split("\n"):
            if "No such file or directory" in line:
                # Get the file
                exec_file = line.split()[3]
                exec_file = exec_file.replace("\"", "").strip()
                # Create the file path
                error_folder = "{}/{}".format(initramfs_path, exec_file)
                L.info("folder: {}. line: {}".format(error_folder, exec_file))
                try:
                    self.util.makedirs(os.path.dirname(error_folder))
                except:
                    # Some folder already exists
                    # Try to manually create folder
                    self.util.makedirs(error_folder)
        return

