import sys
import os
from loguru import logger as L
import wget
import tarfile
import shutil
from util import Util
from Command import Command


""" Image Class
Forfat image preparation (generator case)
and image management (convertor case)
"""
class Image():

    """ Image Object Initialization """
    def __init__(self, util, box_type="alpine"):
        self.box_type = box_type
        self.util = util

    """ setupImage

    Setup image with busyself.box_type environment and target apps
    """
    def setupImage(self):
        app_name = self.util.getConfig('app_name')
        L.info("Start building image with app: {}".format(app_name))
        current_path = os.getcwd()
        # Go to util folder
        os.chdir("utils")
        # Create environment with app
        L.info("Changed to utils path: {}".format(os.getcwd()))
        if self.box_type == "alpine":
            L.info("Setup {}".format(self.box_type))
            cmd = "sudo ./alpine-chroot-install -d alpine -p {} -p strace".format(app_name)
            c = Command(cmd)
            c.run()
        else:
            # Later
            L.info("Setup {}".format(self.box_type))
        L.info("Done!")
        # Go back to original folder
        os.chdir(current_path)
        return

    """ createBaseFoldersInitramfs """
    def createBaseFoldersInitramfs(self):
        # Change directory
        L.info("Create based folder in initramfs")
        current_path = os.getcwd()
        os.chdir("initramfs")
        cmd = "mkdir -p bin sbin etc proc sys usr/bin usr/sbin tmp lib"
        c = Command(cmd)
        c.run()
        # Change back
        os.chdir(current_path)
        return

    """ Cleaning the existing alpine (chroot)

    When setup, the alpine script has mounted /sys /proc, and /dev
    from the host to the debootstrap environment

    Before installing the new environment, we need to remove the alpine folder
    Cleaning alpine requires umount of the previous folder

    Mounted:
        mount: none mounted on alpine/proc.
        mount: /sys bound on alpine/sys.
        mount: /dev bound on alpine/dev.
        mount: lightweight_vm_broker/utils bound on alpine/home/tucn/Workspace/lightweight_vm_broker/utils.
    """
    def cleanAlpine(self):
        # check mount points exists in alpine
        cmd = "mount"
        c = Command(cmd)
        out = c.run()[1].decode("utf-8")
        alpine_path = os.path.join("utils", "alpine", "alpine")
        if "alpine" in out:
            # Neccessary points
            for mount_point in ["dev", "sys", "proc"]:
                cmd = "sudo umount -l {}/{}".format(alpine_path, mount_point)
                c = Command(cmd)
                c.run()
            # Additional points
            cmd = "mount"
            c = Command(cmd)
            extra_points = c.run()[1].decode("utf-8")
            L.info("Get extra points")
            for point in extra_points.split("\n"):
                if "alpine" in point:
                    # Get mounted point
                    ex_point = point.split()[2]
                    cmd = "sudo umount -l {}".format(ex_point)
                    c = Command(cmd)
                    c.run()
            # Change to current user and delete
            # Make sure that the path is never blank
            # We don't delete with code, only move the folder to .del
            L.info("Backup")
            if not os.path.isdir("backup"):
                os.makedirs("backup")
            cmd = "sudo mv {} backup/old_alpine".format(alpine_path)
            c = Command(cmd)
            c.run()
            L.info("Environment has been cleaned")
        else:
            L.info("Environment is clean")
        return


    """ prepareInitramfs
    Prepare folder for initramfs
    """
    def prepareInitramfs(self):
        # Check if there is initramfs folder
        L.info("Preparing initramfs")
        if not os.path.isdir("backup"):
            os.makedirs("backup")
        if os.path.isdir("initramfs"):
            cmd = "sudo mv {} backup/initramfs_old".format("initramfs")
            c = Command(cmd)
            c.run()
            # Don't delete
            # shutil.rmtree("initramfs")
        # Make new folder
        self.util.makedirs("initramfs")
        # Create basic folders
        return

    """ Copy based library on alpine.

    1.musl -- an alternative "libc" version
    """
    def copyBaseLibs(self, initramfs_path, arch="x86_64"):
        lib_dir = os.path.join(initramfs_path, "lib")
        base_lib_dir = os.path.join("base_libs", arch)
        for f in os.listdir(base_lib_dir):
            # Copy to lib folder
            src = os.path.join(base_lib_dir, f)
            dest = os.path.join(lib_dir, f)
            shutil.copy(src, dest)
        return

    """ copyHeuristic. """
    def copyHeuristic(self, initramfs_path, type="alpine"):
        L.info("Copy heuristic data to initramfs folder")
        # Search for heuristic data in FAT system
        app_name = self.util.getConfig("app_name")
        fatsystem_path = self.util.getConfig("fatsystem_path")
        heurtistic_folders = ["etc", "var/log", "var/lib"]
        ret = set()
        # Copy heuristic folders
        for heu in heurtistic_folders:
            search_path = os.path.join(fatsystem_path, heu)
            cmd = "grep -Rn \"{}\" {}".format(app_name, search_path)
            c = Command(cmd)
            out = c.run()[1].decode("utf-8")
            for line in out.split("\n"):
                if ":" not in line:
                    continue
                path = line.split(":")[0]
                # If app_name in path
                # Then there is a dependencies folder
                # Extract an copy all the folder
                if app_name in path:
                    ret.add(os.path.join(path.split(app_name)[0], app_name))
                else:
                    ret.add(path)

            search_path = os.path.join(fatsystem_path, heu)
            cmd = "find {} -name {}".format(search_path, app_name)
            c = Command(cmd)
            out = c.run()[1].decode("utf-8")
            L.info("Finding: {}".format(out))
            for path in out.split("\n"):
                if app_name in path:
                    ret.add(os.path.join(path.split(app_name)[0], app_name))

            for path in ret:
                # Check and create folder if needed
                initramfs_path = path.replace(fatsystem_path, "initramfs/")
                L.info("Path: {}. Initramfs: {}".format(path, initramfs_path))
                # Prepare folder and file
                if not os.path.exists(initramfs_path):
                    # Check the path is file or dir
                    if os.path.isfile(path):
                        # Check the path
                        try:
                            os.makedirs(os.path.dirname(initramfs_path))
                        except:
                            L.info("Folder exists")
                        # Copy
                        try:
                            L.info("Copy: {} to {}".format(path, initramfs_path))
                            shutil.copy(path, initramfs_path)
                        except Exception as e:
                            L.error("Error: {}".format(e))
                    # Copy file
                    else:
                        prev = "{}".format(os.path.sep).join(initramfs_path.split(os.path.sep)[:-1])
                        try:
                            os.makedirs(prev)
                        except:
                            L.info("Folder exists")
                        # Copy
                        try:
                            L.info("Copy: {} to {}".format(path, initramfs_path))
                            shutil.copytree(path, initramfs_path)
                        except Exception as e:
                            # Permission deny, try other method
                            cmd = "sudo cp -r {} {}".format(path, initramfs_path)
                            c = Command(cmd)
                            c.run()
        return

    """ copyData.

    Copy data to initramfs folder
    """
    def copyData(self, data, initramfs_path, type="alpine"):
        L.info("Copy data to initramfs folder")
        full_data = []
        for binary, deps in data.items():
            # Binary is full
            full_data += [binary]
            for dep in deps:
                file_name = dep.split(":")[-1]
                searched_file = dep.split(":")[-1]
                # Check file full location
                full_data += self.util.findInAlpine(searched_file)
        L.info("data: {}".format(full_data))
        fatsystem_path = self.util.getConfig("fatsystem_path")
        for data in full_data:
            # There can be file and directory
            if not data:
                continue
            initramfs_fullfile = data.replace(fatsystem_path, "").strip()
            if initramfs_fullfile[0] == "/":
                initramfs_fullfile = initramfs_fullfile[1:]
            base_folder = os.path.dirname(initramfs_fullfile)
            dest_dir = os.path.join(initramfs_path, base_folder)
            # Check and make folder if needed
            if(os.path.isdir(data)):
                if(not os.path.isdir(dest_dir)):
                    os.makedirs(dest_dir)
                # Data is a folder, don't need to copy
                continue
            # Data is a file
            dest_file = os.path.join(dest_dir, os.path.basename(initramfs_fullfile))
            if(not os.path.isdir(dest_dir)):
                os.makedirs(dest_dir)
            L.info("Copy file: {} to {}".format(data, dest_file))
            try:
                shutil.copy(data, dest_file)
            except Exception as e:
                L.error("Error: {}".format(e))
        self.copyBaseLibs(initramfs_path)
        return
