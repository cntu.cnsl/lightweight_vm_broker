import sys
import os
import re
from loguru import logger as L
import wget
import tarfile
import shutil
from util import Util
from Command import Command

class ModuleHWAnalyzer():

        """Initialization. """
        def __init__(self, util, apps, box_type="alpine"):
            self.box_type = box_type
            self.util = util
            self.apps = apps

        def extractDevInLine(self, line):
            p = re.compile("\/dev\/\w+(\/?\w*)*")
            ret = set()
            matches = p.match(line)
            if matches:
                try:
                        ret |= matches.group(0)
                except:
                        ret.add(matches.group(0))
                return ret
            # Try another way
            dev_post = line.split("/dev")[1]
            dev_post = dev_post.split()[0]
            dev_post = dev_post.split(",")[0]
            dev_post = dev_post.split("`")[0]
            dev_post = dev_post.split(":")[0]
            dev_post = dev_post.split(")")[0]
            dev_post = dev_post.split("(")[0]
            dev_post = dev_post.split("\"")[0]
            dev_str = "/dev{}".format(dev_post)
            ret.add(dev_str)
            return ret

        """ execute and trace an app """
        def traceToDevs(self, app, params, syscalls="open,ioctl,write"):
            L.info("Extract open syscalls from execution: {}. Params: {}".format(app, params))
            L.info("Please input each line with the corresponding inputs")
            fatsystem_path = self.util.getConfig("fatsystem_path")
            full_cmd = "strace --trace={} --output=out.txt {} {}".format(syscalls, app.replace(fatsystem_path, ""), " ".join(params))
            output = self.util.executeInAlpine(full_cmd)
            # Read the output
            L.info("Try to read output file")
            if os.path.isfile(os.path.join(fatsystem_path, "out.txt")):
                with open(os.path.join(fatsystem_path, "out.txt")) as _f:
                    content = _f.readlines()
            devs = set()
            for line in content:
                # Extract dev in line
                if "dev" in line:
                    devs |= self.extractDevInLine(line)
            return devs

        """ object dump  """
        def stringsToDevs(self, app):
            L.info("Strings binary: {}".format(app))
            full_cmd = "strings {}".format(app)
            c = Command(full_cmd)
            out = c.run()[1].decode('utf-8')
            devs = set()
            for line in out.split("\n"):
                if "/dev" in line:
                    devs |= self.extractDevInLine(line)
            return devs

        """ collect /dev/ from trace. """
        def analyzeDevs(self):
            devs = set()
            executor = self.util.getConfig("execution")
            command = executor.split()[0]
            params = executor.split()[1:]
            for app in self.apps:
                L.info("Analyze the app with execution: {}".format(app))
                devs |= self.stringsToDevs(app)
                if command in app:
                    L.info("Dynamic execute app with command: {}".format(command))
                    devs |= self.traceToDevs(app, params)
            return devs

        """ Turn some heuristic check into module."""
        def analyzeWithHeuristic(self):
            initramfs_path = self.util.getConfig("initramfs")
            executor = self.util.getConfig('execution').split()[0]
            traces = {
                    'net':'inet' # network
                    }
            found = set()
            for kmod, trace in traces.items():
                cmd = "objdump {}/{} -T".format(initramfs_path, executor)
                c = Command(cmd)
                outputs = c.run()[1].decode("utf-8")
                for line in outputs.split("\n"):
                    if trace in line:
                        found.add(kmod)
                        continue
            return found


        """ Turn devs information into kernel Module options """
        def turnTracesToModuleOpts(self, devs, heuristics):
            # Read kernel config
            if len(devs) < 1:
                return
            L.info("Turn devs: {} into module option".format(devs))
            k_content = []
            with open("templates/.config_kernel") as _f:
                k_content = _f.readlines()
            matches = {}
            module_rules = ""
            default_modules = ['random', 'urandom']
            for dev in devs:
                L.info("Checking module for device: {}".format(dev))
                # 2nd layer enabled module
                second_layer = "_" + dev.split("/")[2]
                if not second_layer:
                    continue
                if second_layer in default_modules:
                    continue
                if module_rules == "":
                    module_rules += second_layer
                else:
                    module_rules += "|" + second_layer
            if heuristics:
                for heu in heuristics:
                    rule = "_" + heu
                    if rule not in module_rules:
                        if module_rules == "":
                            module_rules += rule
                        else:
                            module_rules += "|" + rule
            # Check for the matched line
            p = re.compile("%s" % module_rules.upper())
            new_content = []
            for line in k_content:
                if "CONFIG_" not in line:
                    new_content.append(line)
                    continue
                match = p.search(line)
                if match:
                    if "is not set" in line:
                        config_name = line.split()[1]
                        new_line = "%s=y\n" % config_name
                        new_content.append(new_line)
                    else:
                        new_content.append(line)
                        continue
                else:
                    new_content.append(line)
                    continue
            # Write to new file
            app_name = self.util.getConfig("app_name")
            with open("templates/{}_config_kernel".format(app_name), 'w+') as _f2:
                for line in new_content:
                    _f2.write(line)
            return
