import sys
import os
from loguru import logger as L
import wget
import tarfile
import shutil
from Command import Command

class Setup():
    """
    @box_type can have one of the following values: "busybox", "docker"
    """
    def __init__(self, util, box_type="busybox"):
        self.util = util
        self.type = box_type

    """ Prepare

    Include some preparation such as download neccessary source code for build
    """
    def earlySetup(self):
        # Get the name of filtered app
        app_name = self.util.getConfig('app_name')
        L.info("Start building app: {}".format(app_name))
        # Download source
        kernel_out, busybox_out = self.downloader()
        # Build
        kernel_build, busybox_build, kernel_src, busybox_src = self.extractor(kernel_out, busybox_out)
        # Compile
        kernel_compile, busybox_compile = self.compile(kernel_build, busybox_build, kernel_src, busybox_src)
        return kernel_compile, busybox_compile

    """ Downloader

    Download kernel and busybox source code
    """
    def downloader(self):
        # Download kernel and busybox
        L.info("Preparing kernels and busybox")
        kernel_ver = self.util.getConfig('kernel_ver')
        kernel_ver_prefix = "v{}.x".format(kernel_ver.split(".")[0])
        kernel_url = "https://cdn.kernel.org/pub/linux/kernel/{}/linux-{}.tar.xz".format(kernel_ver_prefix, kernel_ver)
        kernel_out = os.path.join("downloads","linux-{}.tar.xz".format(kernel_ver))
        busybox_ver = self.util.getConfig('busybox_ver')
        busybox_url = "https://busybox.net/downloads/busybox-{}.tar.bz2".format(busybox_ver)
        busybox_out = os.path.join("downloads", "busybox-{}.tar.bz2".format(busybox_ver))
        # Check download folder
        if not os.path.isdir("downloads"):
            os.mkdir("downloads")
        # Check and download files
        if not os.path.isfile(kernel_out):
            L.info("Kernel file not exists. Downloading with url: {} ".format(kernel_url))
            wget.download(kernel_url, out=kernel_out)
        if not os.path.isfile(busybox_out):
            L.info("Busybox file not exists. Downloading with url: {}".format(busybox_url))
            wget.download(busybox_url, out=busybox_out)
        return kernel_out, busybox_out

    """ Compile

    Compile the kernel and busybox source code
    """
    def compile(self, kernel_build, busybox_build, kernel_src, busybox_src):
        L.info("Compile the kernel and busybox")
        # Check compile folder
        if not os.path.isdir("compile"):
            os.mkdir("compile")
        current_path = os.getcwd()
        # Change to kernel src dir
        os.chdir(kernel_build)
        L.info("Changed to kernel path: {}".format(os.getcwd()))
        kernel_compile = os.path.join(current_path, "compile", kernel_src)
        # Create kernel compile folder, don't need but just in case
        if not os.path.isdir(kernel_compile):
            os.mkdir(kernel_compile)
        cmd = "make O={} allnoconfig".format(kernel_compile)
        L.info("Running command: {}".format(cmd))
        c = Command(cmd)
        c.run()
        # Return
        os.chdir(current_path)
        # Change to busybox src dir
        os.chdir(busybox_build)
        L.info("Changed to busybox path: {}".format(os.getcwd()))
        busybox_compile = os.path.join(current_path, "compile", busybox_src)
        if not os.path.isdir(busybox_compile):
            os.mkdir(busybox_compile)
        cmd = "make O={} allnoconfig".format(busybox_compile)
        L.info("Running command: {}".format(cmd))
        c = Command(cmd)
        c.run()
        # Change back to current path
        os.chdir(current_path)
        return kernel_compile, busybox_compile

    """ Extractor

    Extract the kernel and busybox source code to folder for build
    """
    def extractor(self, kernel_out, busybox_out):
        L.info("Extracting the kernel and busybox")
        # Check build folder
        if not os.path.isdir("build"):
            os.mkdir("build")
        kernel_src = ".".join(kernel_out.split("/")[1].split(".")[:-2])
        busybox_src = ".".join(busybox_out.split("/")[1].split(".")[:-2])
        kernel_build = os.path.join("build", kernel_src)
        busybox_build = os.path.join("build", busybox_src)
        if not os.path.isdir(kernel_build):
            L.info("Extracting kernel tar to build folder")
            tf = tarfile.open(kernel_out)
            tf.extractall("build")
        if not os.path.isdir(busybox_build):
            L.info("Extracting busybox tar to build folder")
            tf = tarfile.open(busybox_out)
            tf.extractall("build")
        # Extracted to folder
        return kernel_build, busybox_build, kernel_src, busybox_src

