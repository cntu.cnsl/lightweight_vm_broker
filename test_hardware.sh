#/bin/bash
### PLEASE DELETE THE INITRAMFS FOLDER BEFORE TEST
echo "List of avaiable folder:"
ls experiments/ex1_2/
echo "Please choose experiment folder to deploy: "
read EXP_FOLDER
## CHECK INPUT_STRING VALID OR NOT
CHECK=$(ls experiments/ex1_2/$EXP_FOLDER | wc -l)
echo "CHECK: $CHECK"
if [ "$CHECK" -eq "0" ]; then
   echo "false";
   exit;
fi
cp -r experiments/ex1_2/$EXP_FOLDER initramfs
cp -r utils/_install/* initramfs/
cp utils/init initramfs
cd utils
./initramfs_compile.sh
./qemu_start.sh
