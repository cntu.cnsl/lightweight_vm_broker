import configparser
import sys
import os
from loguru import logger as L
import wget
import tarfile
import shutil
from Command import Command
from setup import Setup
from image import Image
from util import Util
from image_data_analyzer import ImageDataAnalyzer
from module_hw_analyzer import ModuleHWAnalyzer


"""" Prepare

Prepare for source code build. Include update .config
This preparation step is depending on the image preparation process
"""
def prepareKernel(kernel_build, busybox_build):
    L.info("Preparing the build")
    return

if __name__ == '__main__':
    util = Util()

    # setup_obj = Setup(util)
    # kernel_compile, busybox_compile = setup_obj.earlySetup()

    image_obj = Image(util)
    image_obj.cleanAlpine()
    image_obj.prepareInitramfs()
    image_obj.setupImage()
    image_obj.createBaseFoldersInitramfs()
    image_obj.copyHeuristic("initramfs")
    L.info("Prepared Image")

    img_data_obj = ImageDataAnalyzer(util)
    out = img_data_obj.analyze()
    out2 = img_data_obj.analyzeWithExec()
    L.info("Finished Analyzing")

    module_hw_obj = ModuleHWAnalyzer(util, out)
    devs = module_hw_obj.analyzeDevs()
    heuristics = module_hw_obj.analyzeWithHeuristic()
    print(heuristics)
    module_hw_obj.turnTracesToModuleOpts(devs, heuristics)

    L.debug("Data Analysis Outputs: {}".format(out))
    image_obj.copyData(out, "initramfs")
    print(L.info("Data: {}".format(out)))
